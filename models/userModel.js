var user = {
	username:{type:'string', default:false, required:false},
	phone:{type:'number', default:false, required:true},
	verification_code:{type:'string', default:false, required:true},
	phone_verification:{type:'number', default:0, required:false},
	age:{type:'number', default:false, required:false},
	birth_date:{type:'object', default:false, required:false}
}

module.exports = user