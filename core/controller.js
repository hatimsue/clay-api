
var controller = {
	handler:handler=>{
		let controleFile = `../controllers/${handler.split('@')[0]}.js`
		let method = handler.split('@')[1]
		return require(controleFile)[method]
	}
}


module.exports = controller