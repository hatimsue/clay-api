
var middleware = {
	handler:handler=>{
		let middlewareFile = `../middlewares/${handler.split('@')[0]}.js`
		let method = handler.split('@')[1]
		return require(middlewareFile)[method]
	}
}


module.exports = middleware