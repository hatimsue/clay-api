var service = {
	handler:handler=>{
		let serviceFile = `../services/${handler.split('@')[0]}.js`
		let method = handler.split('@')[1]
		return require(serviceFile)[method]()
	}
}


module.exports = service