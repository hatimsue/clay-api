var clay=require('mongo-clay')
// eslint-disable-next-line no-undef
clay.set(process.env.MONGO_CONECTION,process.env.MONGO_DB)
clay.model = {
	parse:(model,data)=>{
		var document = {}
		for(var a in model){
			if(data[a]){
				if(typeof data[a] == model[a].type){
					document[a] = data[a]
				}else{
					return {err:`${a} is not the same type as in model`}
				}
			}else{
				if(model[a].required==true){
					return {err:`${a} is required`}
				}else{
					document[a] = model[a].default
				}
			}
		}
		return document
	}
}

module.exports = clay

