var express = require('express')
var logger = require('morgan')
var bodyParser = require('body-parser')


var app = express()


app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())

var routes = require('./routes/index')
app.use('/', routes)
app.use(function(req, res) {
	var err = new Error('Not Found')
	err.status = 404
	res.send({err})
})



module.exports = app