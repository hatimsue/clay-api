var jwt = require('jsonwebtoken')

var JWTTokenMiddleware = {
	auth:(req,res, next)=>{
		var token = req.headers['authorization']||''
		// eslint-disable-next-line no-undef
		jwt.verify(token.replace('Bearer ',''),process.env.API_KEY,(err,t)=>{
			if(!err){
				res.status(401).send({
					msg:'forbidden'
				})
			}else{
				
				req.token = t
				next()
			}
		})
	}
}

module.exports = JWTTokenMiddleware